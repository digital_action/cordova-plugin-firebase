#!/usr/bin/env node
'use strict';

var fs = require('fs');

var getValue = function(config, name) {
    var value = config.match(new RegExp('<' + name + '>(.*?)</' + name + '>', "i"))
    if(value && value[1]) {
        return value[1]
    } else {
        return null
    }
}

function fileExists(path) {
  try  {
    return fs.statSync(path).isFile();
  }
  catch (e) {
    return false;
  }
}

function directoryExists(path) {
  try  {
    return fs.statSync(path).isDirectory();
  }
  catch (e) {
    return false;
  }
}


if (directoryExists("platforms/ios")) {
  var config = fs.readFileSync("config.xml").toString()
  var name = getValue(config, "name")
  var paths = ["GoogleService-Info.plist", "platforms/ios/www/GoogleService-Info.plist"];

  for (var i = 0; i < paths.length; i++) {
    if (fileExists(paths[i])) {
      try {
        var contents = fs.readFileSync(paths[i]).toString();
        fs.writeFileSync("platforms/ios/" + name + "/Resources/GoogleService-Info.plist", contents)
      } catch(err) {
        process.stdout.write(err);
      }

      break;
    }
  }

  var paths2 = ["entitlements.plist", "platforms/ios/www/entitlements.plist"];

  for (var i = 0; i < paths2.length; i++) {
    if (fileExists(paths2[i])) {
      try {
        var contents = fs.readFileSync(paths2[i]).toString();
        fs.writeFileSync("platforms/ios/" + name + "/Resources/entitlements.plist", contents)
      } catch(err) {
        process.stdout.write(err);
      }

      break;
    }
  }

  var paths3 = ["Info.plist", "platforms/ios/www/Info.plist"];

  for (var i = 0; i < paths3.length; i++) {
    if (fileExists(paths3[i])) {
      try {
        var contents = fs.readFileSync(paths3[i]).toString();
        fs.writeFileSync("platforms/ios/" + name + "/Resources/Info.plist", contents)
      } catch(err) {
        process.stdout.write(err);
      }

      break;
    }
  }
}

if (directoryExists("platforms/android")) {
  var paths = ["google-services.json" , "platforms/android/assets/www/google-services.json"];

  for (var i = 0; i < paths.length; i++) {
    if (fileExists(paths[i])) {
      try {
        var contents = fs.readFileSync(paths[i]).toString();
        fs.writeFileSync("platforms/android/google-services.json", contents);

        var json = JSON.parse(contents);
        var strings = fs.readFileSync("platforms/android/res/values/strings.xml").toString();

        // strip non-default value
        // strings = strings.replace(new RegExp('<string name="google_app_id">([^\@<]+?)<\/string>', "gi"), '')
        // strings = strings.replace(new RegExp('<string name=\'google_app_id\'>([^\@<]+?)<\/string>', "gi"), '')

        // // strip non-default value
        // strings = strings.replace(new RegExp('<string name="google_api_key">([^\@<]+?)<\/string>', "gi"), '')
        // strings = strings.replace(new RegExp('<string name=\'google_api_key\'>([^\@<]+?)<\/string>', "gi"), '')

        // // strip empty lines
        // strings = strings.replace(new RegExp('(\r\n|\n|\r)[ \t]*(\r\n|\n|\r)', "gm"), '$1')

        // // replace the default value
        // var app_id = '<string name="google_app_id">' + json.client[0].client_info.mobilesdk_app_id + '</string>';
        // var api_key = '<string name="google_api_key">' + json.client[0].api_key[0].current_key + '</string>';
        // strings = strings.replace(new RegExp('<string name="google_app_id">([^<]+?)<\/string>', "gi"), app_id)
        // strings = strings.replace(new RegExp('<string name=\'google_app_id\'>([^<]+?)<\/string>', "gi"), app_id)

        // // replace the default value
        // strings = strings.replace(new RegExp('<string name="google_api_key">([^<]+?)<\/string>', "gi"), api_key)
        // strings = strings.replace(new RegExp('<string name=\'google_api_key\'>([^<]+?)<\/string>', "gi"), api_key)

        // var newStrins = '';
        // var a = strings.split(api_key);
        // for(var i = 0; i < a.length; i++){
        //   newStrins += a[i];
        //   if (i == 0) newStrins += api_key;
        // }
        // strings = newStrins;

        // newStrins = '';
        // a = strings.split(app_id);
        // for(var i = 0; i < a.length; i++){
        //   newStrins += a[i];
        //   if (i == 0) newStrins += app_id;
        // }
        // strings = newStrins;

        fs.writeFileSync("platforms/android/res/values/strings.xml", strings);
        process.stdout.write(strings);
      } catch(err) {
        process.stdout.write(err);
      }

      break;
    }
  }
}
